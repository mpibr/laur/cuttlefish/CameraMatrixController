# CameraMatrixController
Arduino CAN controller to trigger the camera array

#Arduino MEGA 2560 pin layout:

| Camera | Arduino pins |
| --------|--------------|
| 0-4   | 22,23,24,25,26 |
| 5-9   | 27,28,29,30,31 |
| 10-14 | 32,33,34,35,36 |
| 15-19 | 37,42,43,44,45 |
| 20-24 | 46,47,48,49,41 |
