#include <SPI.h>
#include "mcp_can.h"

#define DEBUG 1
#define CANID_ARDUINO_CAMERAMATRIXCHANGED 0x620
#define CANID_ARDUINO_STOP 0x621
#define CANID_ARDUINO_RECORD 0x622

unsigned int initcount;
unsigned long frameIndex = 0;
const int SPI_CS_PIN = 9;
MCP_CAN CAN(SPI_CS_PIN);                                    // Set CS pin
bool recording = false;
unsigned long time_off;
int period = 1000;

//outputRegister 0: PA0-PA7 //22-29
//outputRegister 1: PB0-PB6 //53,52,51,50,10,11,12,13 INTERR
//outputRegister 2: PC0-PC7 //37-30
//outputRegister 3: PL0-PL7 //49-42
//outputRegister 4: PG0-PG7 //41,40,39,XX,XX,04,XX,

//outputRegister 5: PH0-PH7 //17,16,XX,06,07,08,09,XX used for stop signal, all bits set together!


//Maps the camera index to a port register and bit
const int cameraMap[32][2] = {
  {0,0},{0,1},{0,2},{0,3},{0,4}, //pins: 22,23,24,25,26 (cameras 0-4)
  {0,5},{0,6},{0,7},{2,7},{2,6}, //pins: 27,28,29,30,31 (cameras 5-9)
  {2,5},{2,4},{2,3},{2,2},{2,1}, //pins: 32,33,34,35,36 (cameras 10-14)
  {2,0},{3,7},{3,6},{3,5},{3,4}, //pins: 37,42,43,44,45 (cameras 15-19)
  {3,3},{3,2},{3,1},{3,0},{4,0}, //pins: 46,47,48,49,41 (cameras 20-24)
  {4,1},{4,1},{4,1},{4,1},{4,1}, //pins: 40
  {4,1},{4,1}
};

//Maps the ports to the bytes in the can message
const int reverseMap[5][8][2] = {
  {{7,0},{7,1},{7,2},{7,3},{7,4},{7,5},{7,6},{7,7}},
  {{3,7},{3,7},{3,7},{3,7},{3,7},{3,7},{3,7},{3,7}},
  {{6,7},{6,6},{6,5},{6,4},{6,3},{6,2},{6,1},{6,0}},
  {{5,7},{5,6},{5,5},{5,4},{5,3},{5,2},{5,1},{5,0}},
  {{4,0},{3,7},{3,7},{3,7},{3,7},{3,7},{3,7},{3,7}}
};


uint8_t activePorts[5] = {
  B00000000, //determine which ports are active
  B00000000,
  B00000000,
  B00000000,
  B00000000
};

uint8_t lastActivePorts[5] = {
  B00000000, //determine which ports are active
  B00000000,
  B00000000,
  B00000000,
  B00000000
};

void enableCamera(int row, int col);

void setup()
{
  if(DEBUG)
    Serial.begin(9600);
  double timerfreq = 25;
  double deltaT = 1 / (timerfreq*2);
  double cpufreq = 16000000; //in Hz
  double prescale = 256;
  double maxcount=pow(2,16); //16 bit timer
  initcount = maxcount - (deltaT * cpufreq / prescale);
  
  while (CAN_OK != CAN.begin(CAN_500KBPS))              // init can bus : baudrate = 500k
  {
    if(DEBUG){
      Serial.println("CAN BUS Shield init fail");
      Serial.println(" Init CAN BUS Shield again");
    }
    delay(100);
  }
  if(DEBUG)
    Serial.println("CAN BUS Shield init ok!");

  // direction registers (set as output)
  DDRA = 0xFF;
  DDRB = 0xFF; 
  DDRC = 0xFF;
  DDRL = 0xFF;
  DDRH = 0xFF;
  DDRH = 0xFF;

  //set lines to low
  PORTA = 0x00;
  PORTB = 0x00; 
  PORTC = 0x00;
  PORTL = 0x00;
  PORTG = 0x00;
  PORTH = 0x00;

  // Timer 1
  noInterrupts();           // disable all interrupts
  TCCR1A = 0;
  TCCR1B = 0;

  TCNT1 = initcount;        // Initialize timer with calculated value
  TCCR1B |= (1 << CS12);    // set 256 as prescale-value
  TIMSK1 |= (1 << TOIE1);   // activate timer overflow interrupt
  interrupts();             // enable all interrupts

}

// timer overflow interrupt
ISR(TIMER1_OVF_vect)        
{
  TCNT1 = initcount;             // reassign counter
  
  //toggle all pins on port, bitwise exclusive OR
  PORTA ^= (0xFF & activePorts[0]); 
  PORTB ^= (0xFF & activePorts[1]);
  PORTC ^= (0xFF & activePorts[2]);
  PORTL ^= (0xFF & activePorts[3]);
  PORTG ^= (0xFF & activePorts[4]);

//  if(stopRecording==true){ //stop current recording
//    stopRecording = false;
//    PORTH = 0xFF;
//    time_off = millis();
//    Serial.println("pulse on");
//  }else{ 
//     if(millis() > time_off + period){
//      PORTH = 0x00;
//      Serial.println("and off");
//     }
//  }
//  
  if(recording==true){ //stop current recording
      PORTH = 0xFF;
  }else{
    PORTH = 0x00;
  }

  bool notify = false;
  for(int i=0; i<5; i++){
    if(activePorts[i] != lastActivePorts[i]){
      notify = true;
    }
    lastActivePorts[i] = activePorts[i];
  }
  
  if(notify){
    if(DEBUG){
      Serial.println("notifying");
    }
    unsigned char canMsg[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    
    for(int i=0; i<5; i++){
      for(int bitIndex=0;bitIndex<8;bitIndex++){
        if((activePorts[i] & (1 << bitIndex))!=0){
          canMsg[reverseMap[i][bitIndex][0]] |= (1 << reverseMap[i][bitIndex][1]);
        }
      }
    }
    if(DEBUG){
      Serial.println((activePorts[3] & (1 <<3)) !=0); //test on port 3,3 (pin 46)
    }

    //stuff unsigned long = 32 bit into the first 4 bytes
    canMsg[0] = (int)((frameIndex >> 24) & 0xFF) ;
    canMsg[1] = (int)((frameIndex >> 16) & 0xFF) ;
    canMsg[2] = (int)((frameIndex >> 8) & 0XFF);
    canMsg[3] = (int)((frameIndex & 0XFF));
    
    CAN.sendMsgBuf(0x07B, 0, 8, canMsg);

    if(DEBUG){
      Serial.println("sent can message");
    }
  }
  frameIndex++;

  unsigned char len = 0;
  unsigned char buf[8];
  
  if(CAN_MSGAVAIL == CAN.checkReceive())            // check if data coming
  {
      CAN.readMsgBuf(&len, buf);    // read data,  len: data length, buf: data buf

      unsigned int canId = CAN.getCanId();

      switch(canId){
        case CANID_ARDUINO_CAMERAMATRIXCHANGED: //ID
          if(DEBUG){
            Serial.print("Got data from ID: ");          
            Serial.print(canId, HEX);
            Serial.print(" length:");
            Serial.println(len);
          }
          if(len==4){ //ignore messages of wrong length
            int cameraIndex = 31;
            for(int i = 0; i<len; i++)    // print the data
            {
              for(int bitIndex=7;bitIndex>=0;bitIndex--){
                bitWrite(activePorts[cameraMap[cameraIndex][0]], cameraMap[cameraIndex][1], (int)((buf[i] & (1 << bitIndex))));
                cameraIndex--;
              }
            }
          }
          
          break;
      case CANID_ARDUINO_STOP:
       if(DEBUG){
            Serial.println("Got stop signal!");
       }
       recording = false;
       break;

      case CANID_ARDUINO_RECORD:
       if(DEBUG){
            Serial.println("Got record signal!");
       }
       recording = true;
       break;

      }
        
  }

  
}

void loop()
{
}
